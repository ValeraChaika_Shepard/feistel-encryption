﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2TZI
{
    public class FeistelEncoder
    {
        private string fileNameToEncode;
        private byte[] key;
        private string fileToDecode = "needToDecode.txt";

        public FeistelEncoder(string fileNameToEncode, byte[] key)
        {
            this.fileNameToEncode = fileNameToEncode;
            this.key = key;
        }

        public void readFileToEncode()
        {
            string ost = "";

            if (File.Exists(fileToDecode))
                File.Delete(fileToDecode);

            using (StreamReader fs = new StreamReader(fileNameToEncode))
            {
                while (true)
                {
                    //считали строку
                    string line = fs.ReadLine();
                    //если не удалось считать, значит конец файла - значит выходим
                    if (line == null) break;

                    bool endLineFlag = false;

                    //пока считанная строка не закончиться
                    while(line.Length != 0)
                    {
                        if (line == "")
                            break;

                        //текст из 4х символов
                        string textToEncode;
                        //текущая длина строки
                        int lineLength = line.Length;

                        //в строке больше чем 4 символа
                        if(lineLength >= 4)
                        {
                            //Console.WriteLine("БОЛЬШЕ 4х символов в строке");

                            //есть остатк и он не полный
                            if (ost.Length > 0 && ost.Length < 4)
                            {
                                //длина остатка до добавления
                                int ostLen = ost.Length;
                                //дописываем недастающий кусок в остаток
                                ost += line.Substring(0, (4 - ostLen));
                                //Console.WriteLine("остаток: " + ost);
                                //удаляем из считанной строки нужное колич. символов
                                line = line.Remove(0, (4 - ostLen));
                                //обнуляем остаток
                                if (ost.Length == 4)
                                {
                                    //////////////////////////////////
                                    //КОДИРОВКА ost
                                    encoding(BitWorker.byteArrayFromString(ost));
                                    //////////////////////////////////
                                    ost = "";
                                }
                            }
                            //нет остатка
                            else
                            {
                                //считали 4 символа
                                textToEncode = line.Substring(0, 4);
                                //Console.WriteLine("Можно кодировать: " + textToEncode);
                                //удалили 4 символа
                                line = line.Remove(0, 4);
                                //////////////////////////////////
                                //КОДИРУЕМ textToEncode
                                encoding(BitWorker.byteArrayFromString(textToEncode));
                                //////////////////////////////////
                            }

                        }
                        //в строке меньше 4х символов
                        else
                        {
                            //Console.WriteLine("Меньше 4х символов в строке");

                            //есть остатк и он не полный
                            if (ost.Length > 0 && ost.Length < 4)
                            {
                                int amChToFillOst = 4 - ost.Length;
                                if(lineLength >= amChToFillOst)
                                {
                                    ost += line.Substring(0, amChToFillOst);
                                    line = line.Remove(0, amChToFillOst);
                                    if (ost.Length == 4)
                                    {
                                        //////////////////////////////////
                                        //КОДИРОВКА ost
                                        encoding(BitWorker.byteArrayFromString(ost));
                                        //////////////////////////////////
                                        ost = "";
                                    }
                                } else {
                                    ost += line.Substring(0, lineLength);
                                    line = line.Remove(0, lineLength);
                                    if (ost.Length == 4)
                                    {
                                        //////////////////////////////////
                                        //КОДИРОВКА ost
                                        encoding(BitWorker.byteArrayFromString(ost));
                                        //////////////////////////////////
                                        ost = "";
                                    }
                                    //if (endLineFlag == false)
                                    //{
                                    //    using (StreamWriter sw = new StreamWriter(fileToDecode, true, Encoding.Unicode))
                                    //    {
                                    //        sw.Write('\n');
                                    //    }
                                    //    endLineFlag = true;
                                    //}
                                }
                            }
                            //нет остатка
                            else
                            {
                                //записываем в остаток
                                ost += line.Substring(0, lineLength);
                                //Console.WriteLine("Дописали строку в остаток: " + ost);
                                if(ost.Length == 4)
                                {
                                    ost = "";
                                    //////////////////////////////////
                                    //КОДИРОВКА ost
                                    encoding(BitWorker.byteArrayFromString(ost));
                                    //////////////////////////////////
                                }
                                line = line.Remove(0, lineLength);

                                //if (endLineFlag == false)
                                //{
                                //    using (StreamWriter sw = new StreamWriter(fileToDecode, true, Encoding.Unicode))
                                //    {
                                //        sw.Write('\n');
                                //    }
                                //    endLineFlag = true;
                                //}
                            }


                        }
                    }

                    Console.WriteLine(line);
                    //записываем в файл перенос строки

                }
            }


            if(ost.Length > 0 && ost.Length < 4)
            {
                int curLength = ost.Length;
                int dif = 4 - curLength;
                for (int i = 0; i < dif; i++)
                    ost += '*';
            }
            //////////////////////////////////
            //КОДИРОВКА ost
            encoding(BitWorker.byteArrayFromString(ost));
            //////////////////////////////////
            //Console.WriteLine("Конечный остаток - " + ost);

        }

        public void encoding(byte[] sourceTextInBytes)
        {
            Console.WriteLine("То что было (64): " + BitWorker.bitStringFromByteArray(sourceTextInBytes));
            Console.WriteLine("Если перевести " + BitWorker.textFromByteArray(sourceTextInBytes));

            byte[] Left = new byte[sourceTextInBytes.Length / 2];
            byte[] Right = new byte[sourceTextInBytes.Length / 2];

            for (int i = 0; i < sourceTextInBytes.Length / 2; i++)
            {
                Left[i] = sourceTextInBytes[i];
            }
            for(int i = sourceTextInBytes.Length / 2; i < sourceTextInBytes.Length; i++)
            {
                Right[i - sourceTextInBytes.Length / 2] = sourceTextInBytes[i];
            }

            Console.WriteLine("Левый (32): " + BitWorker.bitStringFromByteArray(Left));
            Console.WriteLine("Если перевести ЛЕВЫЙ: " + BitWorker.textFromByteArray(Left));


            Console.WriteLine("Правый (32): " + BitWorker.bitStringFromByteArray(Right));
            Console.WriteLine("Если перевести ПРАВЫЙ: " + BitWorker.textFromByteArray(Right));

            Console.WriteLine("Ключ: " + BitWorker.bitStringFromByteArray(key));
            Console.WriteLine("--------------------------");

            startRaunds(Left, Right);
        }

        private void startRaunds(byte[] startLeft, byte[] startRight)
        {
            //if(startLeft == null || startRight == null)
            //    return;

            byte[] curKey = key;
            byte[] curLeft = startLeft;
            byte[] curRight = startRight;

            byte[] leftFunc;
            byte[] newCurLeft;

            for (int i = 0; i < 15; i++)
            {
                curKey = keyShiftRight(curKey, 2);
                leftFunc = funcWithKey(curLeft, curKey);
                Console.WriteLine("XOR left + key: " + BitWorker.bitStringFromByteArray(leftFunc));
                newCurLeft = operationXOR(leftFunc, curRight);
                Console.WriteLine("XOR afterFunc + right: " + BitWorker.bitStringFromByteArray(newCurLeft));

                //правый и левый для след итерации
                curRight = curLeft;
                curLeft = newCurLeft;
                Console.WriteLine("NEW RIGHT = " + BitWorker.bitStringFromByteArray(curRight));
                Console.WriteLine("NEW LEGT = " + BitWorker.bitStringFromByteArray(curLeft));
                //curKey = keyShift(curKey, 2);
                Console.WriteLine("NEW KEY AFTER SHIFT 2: " + BitWorker.bitStringFromByteArray(curKey) + " KEYEND;");
            }
            //последняя итерация
            curKey = keyShiftRight(curKey, 2);
            leftFunc = funcWithKey(curLeft, curKey);
            Console.WriteLine("XOR left + key: " + BitWorker.bitStringFromByteArray(leftFunc));
            newCurLeft = operationXOR(leftFunc, curRight);
            Console.WriteLine("XOR afterFunc + right: " + BitWorker.bitStringFromByteArray(newCurLeft));
            curRight = newCurLeft;
            Console.WriteLine("NEW KEY AFTER SHIFT 2: " + BitWorker.bitStringFromByteArray(curKey) + " KEYEND;");
            //curLeft = curLeft;
            writeToFile(curLeft, curRight);

        }

        private void writeToFile(byte[] left, byte[] right)
        {
            using (StreamWriter sw = new StreamWriter(fileToDecode, true, Encoding.Unicode))
            {
                sw.Write(BitWorker.textFromByteArray(left));
                Console.WriteLine("lklklk:" + BitWorker.bitStringFromByteArray(left));
                Console.WriteLine("llllll: " + BitWorker.textFromByteArray(left));
                sw.Write(BitWorker.textFromByteArray(right));
                Console.WriteLine("rkrkrkrk:" + BitWorker.bitStringFromByteArray(right));
                Console.WriteLine("rrrrrr: " + BitWorker.textFromByteArray(right));
            }
        }

        private byte[] keyShiftRight(byte[] key, int amToShift)
        {
            string binCode = BitWorker.bitStringFromByteArray(key);
            string lastSymbols = binCode.Substring(binCode.Length - amToShift);
            Console.WriteLine("LAST SYMBOLS: " + lastSymbols);

            string newKey = "";
            newKey += lastSymbols;

            for(int i = 0; i < binCode.Length - amToShift; i++)
            {
                newKey += binCode[i];
            }

            Console.WriteLine("WTF?: " + newKey);

            byte[] keyBytes = new byte[4];

            for (int i = 0; i < 4; i++)
            {
                string subString = newKey.Substring(0, 8);
                int byteValue = BitWorker.getIntFromStrBits(subString);
                keyBytes[i] = (byte)byteValue;
                newKey = newKey.Remove(0, 8);
            }

            return keyBytes;
        }

        private byte[] funcWithKey(byte[] left, byte[] key)
        {
            return BitWorker.myXOR(left, key);
        }

        private byte[] operationXOR(byte[] left, byte[] right)
        {
            return BitWorker.myXOR(left, right);
        }






    }
}
