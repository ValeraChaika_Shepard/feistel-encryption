﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2TZI
{
    public class BitWorker
    {
        public static byte[] byteArrayFromString(string text)
        {
            return Encoding.Unicode.GetBytes(text.ToCharArray());
        }

        public static string bitStringFromByteArray(byte[] bytes)
        {
            if (bytes == null)
                return "\0";
            return string.Join("", bytes.Select(x => Convert.ToString(x, 2).PadLeft(8, '0')));
        }

        public static string textFromByteArray(byte[] bytes)
        {
            if (bytes == null)
                return "\0";
            return Encoding.Unicode.GetString(bytes);
        }

        public static int getIntFromStrBits(string binText)
        {
            int num = 0;
            char[] tmp = binText.ToCharArray();
            int j = 0;
            for (int i = tmp.Length - 1; i >= 0; i--, j++)
                if (tmp[i] != '0')
                    num += (int)Char.GetNumericValue(tmp[i]) * (int)Math.Pow(2, j);
            return num;
        }

        public static byte[] myXOR(byte[] first, byte[] second)
        {
            try
            {
                byte[] res = new byte[first.Length];

                int firstInt = BitConverter.ToInt32(first, 0);
                int secondInt = BitConverter.ToInt32(second, 0);

                int final = firstInt ^ secondInt;

                res = BitConverter.GetBytes(final);

                return res;
            }
            catch(Exception e){ return null; }
        }


        //public static string BitArrayToString(BitArray bits)
        //{
        //    var sb = new StringBuilder();

        //    for (int i = bits.Count-1; i >= 0; i--)
        //    {
        //        char c = bits[i] ? '1' : '0';
        //        sb.Append(c);
        //    }

        //    return sb.ToString();
        //}


        //public static byte[] GetBytes(string bitString)
        //{
        //    return Enumerable.Range(0, bitString.Length / 8).
        //        Select(pos => Convert.ToByte(bitString.Substring(pos * 8, 8),2)
        //        ).ToArray();
        //}

    }
}
