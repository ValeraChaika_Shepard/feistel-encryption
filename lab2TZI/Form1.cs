﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2TZI
{
    public partial class Form1 : Form
    {
        private string fileNameToEncode = "";
        private string fileToDecode = "";
        FeistelEncoder fe;
        FeistelDecoder fd;

        public Form1()
        {
            InitializeComponent();
        }

        private void выбратьФайлДляШифрованияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            fileNameToEncode = "";
            fileNameToEncode += openFileDialog1.FileName;
        }

        private void шифровкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(fileNameToEncode.Length == 0)
            {
                MessageBox.Show("Выберита файл для шифровки");
                return;
            }

            bool keyValid = checkKey();

            if (keyValid) {
                string keyText = tbKey.Text.Trim(' ');
                byte[] keyBytes = BitWorker.byteArrayFromString(keyText);
                fe = new FeistelEncoder(fileNameToEncode, keyBytes);
                fe.readFileToEncode();
            }
                
        }


        private bool checkKey()
        {
            string keyText = tbKey.Text;
            keyText = keyText.Trim(' ');

            if(keyText.Length == 0) {
                MessageBox.Show("Введите ключ!");
                return false;
            } else if(keyText.Length > 2) {
                MessageBox.Show("Слишком длинный ключ! (нужно 2 символа)");
                return false;
            } else if(keyText.Length < 2) {
                MessageBox.Show("Слишком короткий ключ! (нужно 2 символа)");
                return false;
            }
            return true;
        }

        private void дешифровкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            fileToDecode = "";
            fileToDecode += openFileDialog1.FileName;

            if (fileToDecode.Length == 0)
            {
                MessageBox.Show("Выберита файл для дешифровки");
                return;
            }

            bool keyValid = checkKey();

            if (keyValid)
            {
                string keyText = tbKey.Text.Trim(' ');
                byte[] keyBytes = BitWorker.byteArrayFromString(keyText);
                fd = new FeistelDecoder(fileToDecode, keyBytes);
                fd.readFileToDecode();
            }
        }
    }
}
