﻿namespace lab2TZI
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выбратьФайлДляШифрованияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шифрованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шифровкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дешифровкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.шифрованиеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(427, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выбратьФайлДляШифрованияToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // выбратьФайлДляШифрованияToolStripMenuItem
            // 
            this.выбратьФайлДляШифрованияToolStripMenuItem.Name = "выбратьФайлДляШифрованияToolStripMenuItem";
            this.выбратьФайлДляШифрованияToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.выбратьФайлДляШифрованияToolStripMenuItem.Text = "Выбрать файл для шифрования";
            this.выбратьФайлДляШифрованияToolStripMenuItem.Click += new System.EventHandler(this.выбратьФайлДляШифрованияToolStripMenuItem_Click);
            // 
            // шифрованиеToolStripMenuItem
            // 
            this.шифрованиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.шифровкаToolStripMenuItem,
            this.дешифровкаToolStripMenuItem});
            this.шифрованиеToolStripMenuItem.Name = "шифрованиеToolStripMenuItem";
            this.шифрованиеToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.шифрованиеToolStripMenuItem.Text = "Фейстель";
            // 
            // шифровкаToolStripMenuItem
            // 
            this.шифровкаToolStripMenuItem.Name = "шифровкаToolStripMenuItem";
            this.шифровкаToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.шифровкаToolStripMenuItem.Text = "Шифровка";
            this.шифровкаToolStripMenuItem.Click += new System.EventHandler(this.шифровкаToolStripMenuItem_Click);
            // 
            // дешифровкаToolStripMenuItem
            // 
            this.дешифровкаToolStripMenuItem.Name = "дешифровкаToolStripMenuItem";
            this.дешифровкаToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.дешифровкаToolStripMenuItem.Text = "Дешифровка";
            this.дешифровкаToolStripMenuItem.Click += new System.EventHandler(this.дешифровкаToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите ключ (2 символа):";
            // 
            // tbKey
            // 
            this.tbKey.Location = new System.Drawing.Point(231, 94);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(47, 20);
            this.tbKey.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 178);
            this.Controls.Add(this.tbKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторная работа №2 Чайка В.";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выбратьФайлДляШифрованияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шифрованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шифровкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дешифровкаToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbKey;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

